<?php
// This module should logout the user. Unset any $_SESSION variables, destroy the session.
session_start();

$_SESSION = array();

session_destroy();
echo "You have been logged out";

echo '. Click the link to return to <a href="login.html"> login page.</a>';


?>